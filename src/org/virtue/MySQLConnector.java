/**
 * Copyright (c) 2014 Virtue Studios
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.virtue;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import org.virtue.utility.PasswordHash;

/**
 * @author Im Frizzy <skype:kfriz1998>
 * @author Frosty Teh Snowman <skype:travis.mccorkle>
 * @author Arthur <skype:arthur.behesnilian>
 * @author Sundays211
 * @since Nov 27, 2014
 */
public class MySQLConnector {

	public static int processLogin(String username, String password, boolean email) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection("jdbc:mysql://rspsdev.com/rspsdev_VRS3Comm", "rspsdev_VRS3Comm", "a$3rg-WNDS8La#ZN-");
				Statement stmt = conn.createStatement();
					ResultSet rs = stmt.executeQuery("SELECT * FROM l3siuhwno6_users WHERE " + (email ? "email" : "user_login") + " = '" + username + "'");
					while (rs.next()) {
					String pass = rs.getString("user_pass");
						System.out.println(pass + ", " + password);
						if (new PasswordHash().isMatch(password, pass)) {
							return 2;
						} else {
							return 1;
						}
					}
					return 0;
					} catch (Exception e) {
						e.printStackTrace();
						return -1;
					}
	}
	
	public static int processLogin1(String username, String password, boolean email) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection("jdbc:mysql://virtuers3.com/rspsdev_VRS3Comm", "rspsdev_VRS3Comm", "a$3rg-WNDS8La#ZN-");
			try {
				Statement stmt = conn.createStatement();
				try {
					String query = "SELECT * FROM l3siuhwno6_users WHERE " + (email ? "email" : "user_login") + " = '" + username + "'";
					System.out.println(query);
					ResultSet rs = stmt.executeQuery("SELECT * FROM l3siuhwno6_users WHERE " + (email ? "email" : "user_login") + " = '" + username + "'");
					try {
						String pass = rs.getString("password");
						System.out.println(pass);
						if (pass.equals(password)) {
							return 2;
						} else {
							return 1;
						}
					} finally {
						try {
							rs.close();
						} catch (Throwable ignore) {
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					return 0;
				} finally {
					try {
						stmt.close();
					} catch (Throwable ignore) {
					}
				}
			} finally {
				try {
					conn.close();
				} catch (Throwable ignore) {
				}
			}
		} catch (Exception e) {
		}
		return -1;
	}
}
