/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013 Oracle and/or its affiliates. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License"). You
 * may not use this file except in compliance with the License. You can
 * obtain a copy of the License at
 * https://glassfish.dev.java.net/public/CDDL+GPL_1_1.html
 * or packager/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at packager/legal/LICENSE.txt.
 *
 * GPL Classpath Exception:
 * Oracle designates this particular file as subject to the "Classpath"
 * exception as provided by Oracle in the GPL Version 2 section of the License
 * file that accompanied this code.
 *
 * Modifications:
 * If applicable, add the following below the License Header, with the fields
 * enclosed by brackets [] replaced by your own identifying information:
 * "Portions Copyright [year] [name of copyright owner]"
 *
 * Contributor(s):
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package org.virtue.login;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;

import org.virtue.ControlledScreen;
import org.virtue.MySQLConnector;
import org.virtue.ScreensController;
import org.virtue.Virtue;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Kyle Friz
 */
public class LoginController implements Initializable, ControlledScreen {

	/**
	 * The Parent Controller
	 */
	ScreensController myController;

	/**
	 * The Username Text Field
	 */
	@FXML
	private TextField username;

	/**
	 * The Password Text Field
	 */
	@FXML
	private PasswordField password;

	/**
	 * The Remember Check Box
	 */
	@FXML
	private CheckBox remember;

	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) { }

	/**
	 * Sets the Parent Controller
	 */
	public void setScreenParent(ScreensController screenParent) {
		myController = screenParent;
	}

	/**
	 * Opens up the specified webpage
	 * 
	 * @param event
	 */
	@FXML
	private void openHomePage(ActionEvent event) {
		try {
			Desktop.getDesktop().browse(new URI("http://www.virtuers3.com"));
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Processes a login request
	 * 
	 * @param event
	 */
	@FXML
	private void processLogin(ActionEvent event) {
		String user = username.getText();
		String pass = password.getText();
		if (user.equalsIgnoreCase("") || pass.equalsIgnoreCase(""))
			return;
		else if (user.equalsIgnoreCase("username") || pass.equalsIgnoreCase("password"))
			return;
		boolean email = user.contains("@");
		int response = MySQLConnector.processLogin(user, pass, email);
		System.out.println(user + ", " + pass + ", " + response);
		switch (response) {
		case -1:
			/** Error */
			return;
		case 0:
			/** Account Does not Exist */
			return;
		case 1:
			/** Password Does not Match */
			return;
		case 2:
			/** Password Matches */
			break;
		}
		if (remember.isSelected()) {
			/** TODO: Create a system file that holds the username */
		}
		myController.setScreen(Virtue.home);
	}
}